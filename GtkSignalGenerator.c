/* 
 
    GtkSignalGenerator is a tool to generate arbitrary waveforms through
    the soundcard.
    
    
    GtkSignalGenerator v.0.2
    Copyright 2015, 2016, 2019 Vladimir Smolyar <v_2e@ukr.net>
 
    This file is part of the SpectroPhot program.

    SpectroPhot is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SpectroPhot is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SpectroPhot.  If not, see <http://www.gnu.org/licenses/>.
 
    
    http://users.suse.com/~mana/alsa090_howto.html#sect02
 */
 
#define M_PI   3.14159265358979323846   /* pi */

#include <gtk/gtk.h>
#include <gdk/gdk.h>
#include <alsa/asoundlib.h>
#include <math.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

enum available_waveforms {SINE, SQUARE, SAWTOOTH, RAMP, TRIANGLE, PULSE};

//struct signal_params_t {
    //enum available_waveforms waveform;
    //float frequency;
    //float duty;
//};

typedef struct {
    enum available_waveforms left_waveform;
    float left_amplitude;
    float left_frequency;
    float left_duty;
    float left_phase;
    enum available_waveforms right_waveform;
    float right_amplitude;
    float right_frequency;
    float right_duty;
    float right_phase;
    int *stop_sound;
} signal_params_t;

__thread int stop_sound_global = FALSE; // Thread-local variable

//void sine_wave(snd_pcm_t *pcm_handle, snd_pcm_hw_params_t *hwparams, double frequency, double amplitude, double length)
void sine_wave_sound(snd_pcm_t *pcm_handle, double frequency, double amplitude, double length)
{
    g_message("sine_wave_sound called: frequency = %.2f, amplitude = %.1f, length = %.1f", frequency, amplitude, length);
    
    unsigned char *data;
    int pcmreturn, l1, l2;
    short s1, s2;
    int frames;
    unsigned int depth = 16;
    unsigned int rate;
    
    snd_pcm_hw_params_t *hwparams;
    /* Allocate the snd_pcm_hw_params_t structure on the stack. */
    snd_pcm_hw_params_alloca(&hwparams);
    if (snd_pcm_hw_params_current(pcm_handle, hwparams) < 0)
    {
        g_message("Error retrieving current HW params.");
    }
    
    
    if (snd_pcm_hw_params_get_rate(hwparams, &rate, NULL) <0)
    {
        g_message("Error retrieving current rate.");
    }
    
    snd_pcm_uframes_t periodsize;
    snd_pcm_hw_params_get_period_size(hwparams, &periodsize, NULL);
    
    data = (unsigned char *)malloc(periodsize);
    frames = periodsize >> 2;
    //double frequency = 440.0, amplitude = 0.5;
    
    /*
    for(l1 = 0; l1 < (length*rate/frames); l1++) {
      for(l2 = 0; l2 < frames; l2++) {
        s1 = floor(amplitude*pow(2,depth)*sin(2*M_PI*(l1*frames+l2)/(rate/frequency))); // sine wave
        s2 = floor(amplitude*pow(2,depth)*sin(2*M_PI*(l1*frames+l2)/(rate/frequency))); // sine wave
        data[4*l2] = (unsigned char)s1;
        data[4*l2+1] = s1 >> 8;
        data[4*l2+2] = (unsigned char)s2;
        data[4*l2+3] = s2 >> 8;
      }
      while ((pcmreturn = snd_pcm_writei(pcm_handle, data, frames)) < 0) {
        snd_pcm_prepare(pcm_handle);
        fprintf(stderr, "<<<<<<<<<<<<<<< Buffer Underrun >>>>>>>>>>>>>>>\n");
      }
    }
    */
    
    float t = 0;
    //while (t < length)
    while (TRUE)
    {
        for(l2 = 0; l2 < frames; l2++) 
        {
            t += 1.0/rate;
        s1 = floor(amplitude*pow(2,depth)*sin(2*M_PI*frequency*t)); // sine wave
        s2 = floor(amplitude*pow(2,depth)*sin(2*M_PI*frequency*t)); // sine wave
        data[4*l2] = (unsigned char)s1;
        data[4*l2+1] = s1 >> 8;
        data[4*l2+2] = (unsigned char)s2;
        data[4*l2+3] = s2 >> 8;
      }
      while ((pcmreturn = snd_pcm_writei(pcm_handle, data, frames)) < 0) {
        snd_pcm_prepare(pcm_handle);
        fprintf(stderr, "<<<<<<<<<<<<<<< Buffer Underrun >>>>>>>>>>>>>>>\n");
      }

    }
    
    g_message("---===---");    
}

double sine_wave(double t, double frequency, double phase)
{
    return sin(2*M_PI*frequency*t + phase); // sine wave
}

double sawtooth_wave(double t, double frequency, double phase)
{
    // TODO truncate initial phase, then truncate the current time, that sum them and truncate again
   // phase -= floor(phase*frequency)/frequency;  // remove an even number of periods from the initial phase
    //t += phase;
    //t -= floor(t*frequency)/frequency;          // remove an even number of periods from the total phase
    t = fmod(t,1/frequency);

    if (t < 1/(2*frequency)) return 2*t*frequency;     // sawtooth wave
    else return (-2 + 2*t*frequency);
}

double ramp_wave(double t, double frequency, double phase)
{
    // reverse sawtooth wave
    // TODO truncate initial phase, then truncate the current time, that sum them and truncate again
    phase -= floor(phase*frequency)/frequency;  // remove an even number of periods from the initial phase
    t += phase;
    t -= floor(t*frequency)/frequency;          // remove an even number of periods from the total phase

    if (t < 1/(2*frequency)) return (-2*t*frequency);     // sawtooth wave
    else return (2 - 2*t*frequency);
}

double square_wave(double t, double frequency, double phase)
{
    // TODO truncate initial phase, then truncate the current time, that sum them and truncate again
    phase -= floor(phase*frequency)/frequency;  // remove an even number of periods from the initial phase
    t += phase;
    t -= floor(t*frequency)/frequency;          // remove an even number of periods from the total phase

    if (t < 1/(2*frequency)) return 1.0;        // square wave
    else return -1.0;
}

double triangle_wave(double t, double frequency, double phase)
{
    // TODO truncate initial phase, then truncate the current time, that sum them and truncate again
    phase -= floor(phase*frequency)/frequency;  // remove an even number of periods from the initial phase
    t += phase;
    t -= floor(t*frequency)/frequency;          // remove an even number of periods from the total phase

    if (t < 1/(4*frequency)) return 4*t*frequency;       // triangular wave
    else if (t < 3/(4*frequency)) return (1.0 - 4*(t-1/(4*frequency))*frequency);
    else return (-1.0 + 4*(t-3/(4*frequency))*frequency);
}

double pulse_wave(double t, double frequency, double phase, double duty)
{
    // TODO truncate initial phase, then truncate the current time, that sum them and truncate again
    phase -= floor(phase*frequency)/frequency;  // remove an even number of periods from the initial phase
    t += phase;
    t -= floor(t*frequency)/frequency;          // remove an even number of periods from the total phase

    if (t < duty) return 1.0;                   // pulse wave
    else return 0.0;
}

//int main()
//int 
//static void 
//snd_pcm_t * setup_alsa(GtkWidget *widget, gpointer   pdata)
snd_pcm_t * alsa_setup()
{
    g_message("alsa_setup called");
    //gtk_button_set_label ((GtkButton *)widget,"Playing...");
    
    //gdk_window_set_debug_updates (TRUE);
   // GtkWidget *buttonbox, *window;
   // GtkWidget *buttonbox;
    
    //buttonbox = gtk_widget_get_parent(widget);
    //GtkWindow * window = (GtkWindow *) gtk_widget_get_parent(buttonbox);
    
    //gtk_widget_queue_draw_area (widget, 0, 0, 20, 20);
    
    //GdkWindow *window = gtk_widget_get_parent_window (widget);
    
    //gdk_window_invalidate_rect(window, NULL, TRUE);
    //gdk_window_process_updates(window, TRUE);
    //gdk_window_process_all_updates();
    
  /* Handle for the PCM device */ 
    snd_pcm_t *pcm_handle;          

    /* Playback stream */
    snd_pcm_stream_t stream = SND_PCM_STREAM_PLAYBACK;

    /* This structure contains information about    */
    /* the hardware and can be used to specify the  */      
    /* configuration to be used for the PCM stream. */ 
    snd_pcm_hw_params_t *hwparams;
    
    
    /* Name of the PCM device, like plughw:0,0          */
    /* The first number is the number of the soundcard, */
    /* the second number is the number of the device.   */
    char *pcm_name;
    
    /* Init pcm_name. Of course, later you */
    /* will make this configurable ;-)     */
    //pcm_name = strdup("default");
    //pcm_name = strdup("hw:0,0"); // WORKS!!! -- requires an exclusive access to soundcard
    //pcm_name = strdup("dmixplug"); // also works, but requires the parameters detection
    //pcm_name = strdup("dmixer");
    //pcm_name = "dmixer";
    pcm_name = "default";

    /* Allocate the snd_pcm_hw_params_t structure on the stack. */
    snd_pcm_hw_params_alloca(&hwparams);
  
  
    /* Open PCM. The last parameter of this function is the mode. */
    /* If this is set to 0, the standard mode is used. Possible   */
    /* other values are SND_PCM_NONBLOCK and SND_PCM_ASYNC.       */ 
    /* If SND_PCM_NONBLOCK is used, read / write access to the    */
    /* PCM device will return immediately. If SND_PCM_ASYNC is    */
    /* specified, SIGIO will be emitted whenever a period has     */
    /* been completely processed by the soundcard.                */
    
    
    if (snd_pcm_open(&pcm_handle, pcm_name, stream, 0) < 0) 
    //if (snd_pcm_open(&pcm_handle, "default", stream, 0) < 0) 
    {
      fprintf(stderr, "Error opening PCM device %s\n", pcm_name);
      g_message("Error opening PCM device %s", pcm_name);
      //return(-1);
      //GtkWidget * msg = gtk_message_dialog_new (window,
                        //GTK_DIALOG_MODAL,
                        //GTK_MESSAGE_ERROR,
                        //GTK_BUTTONS_CLOSE,
                        //"Error opening PCM device %s", pcm_name);
        //gtk_widget_show (msg);
        //return(-1);
        return NULL;
        //exit(-1);
    }
    
    //g_message("---===---");
    /* Init hwparams with full configuration space */
    if (snd_pcm_hw_params_any(pcm_handle, hwparams) < 0) {
      fprintf(stderr, "Can not configure this PCM device.\n");
      g_message("Can not configure this PCM device.");
      //return(-1);
      exit(-1);
    }
    
    
    unsigned int desired_rate = 44100;  /* Sample rate */ // 192000 , 44100, 96000, 48000
    unsigned int rate;  /* Sample rate returned by */
                        /* snd_pcm_hw_params_set_rate_near */ 
    int dir;            /* rate == desired_rate --> dir = 0 */
                        /* rate < desired_rate  --> dir = -1 */
                        /* rate > desired_rate  --> dir = 1 */
    //int periods = 2;       /* Number of periods */
    int periods = 8;       /* Number of periods */
    snd_pcm_uframes_t periodsize = 8192; /* Periodsize (bytes) */
    //snd_pcm_uframes_t periodsize = 1024; /* Periodsize (bytes) */
    //snd_pcm_uframes_t periodsize = 1; /* Periodsize (bytes) */
    
    /* Set access type. This can be either    */
    /* SND_PCM_ACCESS_RW_INTERLEAVED or       */
    /* SND_PCM_ACCESS_RW_NONINTERLEAVED.      */
    /* There are also access types for MMAPed */
    /* access, but this is beyond the scope   */
    /* of this introduction.                  */
    if (snd_pcm_hw_params_set_access(pcm_handle, hwparams, SND_PCM_ACCESS_RW_INTERLEAVED) < 0) {
      fprintf(stderr, "Error setting access.\n");
      g_message("Error setting access.");
      //return(-1);
      exit(-1);
    }

    unsigned int depth = 16;
    /* Set sample format */
    if (snd_pcm_hw_params_set_format(pcm_handle, hwparams, SND_PCM_FORMAT_S16_LE) < 0) {
      fprintf(stderr, "Error setting format.\n");
      g_message("Error setting format.");
      //return(-1);
      exit(-1);
    }

    /* Set sample rate. If the exact rate is not supported */
    /* by the hardware, use nearest possible rate.         */ 
    rate = desired_rate;
    if (snd_pcm_hw_params_set_rate_near(pcm_handle, hwparams, &rate, 0) < 0) {
      fprintf(stderr, "Error setting rate.\n");
      g_message("Error setting rate.");
      //return(-1);
      exit(-1);
    }
    if (desired_rate != rate) {
      fprintf(stderr, "The rate %d Hz is not supported by your hardware.\n \
                       ==> Using %d Hz instead.\n", desired_rate, rate);
    }

    /* Set number of channels */
    if (snd_pcm_hw_params_set_channels(pcm_handle, hwparams, 2) < 0) {
      fprintf(stderr, "Error setting channels.\n");
      g_message("Error setting channels.");
      //return(-1);
      exit(-1);
    }

    /* Set number of periods. Periods used to be called fragments. */ 
    //if (snd_pcm_hw_params_set_periods_last(pcm_handle, hwparams, &periods, NULL) < 0)
    if (snd_pcm_hw_params_set_periods_first(pcm_handle, hwparams, &periods, NULL) < 0)
    {
        g_error("Could not set periods_first.");
    }
    else
    {
        g_message("Using periods: %d",periods);
    }


    //if (snd_pcm_hw_params_set_periods(pcm_handle, hwparams, periods, 0) < 0) {
      //fprintf(stderr, "Error setting periods.\n");
      //g_message("Error setting periods.");
      ////return(-1);
      //exit(-1);
    //}
    
    
    //unsigned int buffersize = (periods*periodsize)>>2;
    snd_pcm_uframes_t buffersize = (periods*periodsize)>>2;
    //for (unsigned int i = periodsize; i <= 4294967296; i+=periodsize)
    //{
        ////fprintf(stderr,"Probing buffer size: %d\n",i>>2);
        //g_message("Probing buffer size: %d",i>>2);
        //if (snd_pcm_hw_params_test_buffer_size(pcm_handle,hwparams, i>>2) == 0)
        //{
            //buffersize = i>>2;
            //fprintf(stderr,"Buffer size: %d 4-byte frames\n",buffersize);
            //g_message("Buffer size: %d 4-byte frames",buffersize);
            //break;
        //}
    //}
    
    
    /* Set buffer size (in frames). The resulting latency is given by */
    /* latency = periodsize * periods / (rate * bytes_per_frame)     */
    //if (snd_pcm_hw_params_set_buffer_size(pcm_handle, hwparams, (periodsize * periods)>>2) < 0) {
    //if (snd_pcm_hw_params_set_buffer_size(pcm_handle, hwparams, buffersize) < 0)
    if (snd_pcm_hw_params_set_buffer_size_first(pcm_handle, hwparams, &buffersize) < 0)
    {
      fprintf(stderr, "Error setting buffersize.\n");
      g_message("Error setting buffersize.");
      //return(-1);
      exit(-1);
    }
    g_message("Buffer size: %d 4-byte frames",(int)buffersize);
    
    /* Apply HW parameter settings to */
    /* PCM device and prepare device  */
    if (snd_pcm_hw_params(pcm_handle, hwparams) < 0) {
      fprintf(stderr, "Error setting HW params.\n");
      g_message("Error setting HW params.");
      //return(-1);
      exit(-1);
    }
    
 
    return pcm_handle;
}

void make_sound(snd_pcm_t *pcm_handle, signal_params_t *signal_params)
{
    g_message("make_sound called");
       /* Write num_frames frames from buffer data to    */ 
    /* the PCM device pointed to by pcm_handle.       */
    /* Returns the number of frames actually written. */
    //snd_pcm_sframes_t snd_pcm_writei(pcm_handle, data, num_frames);
    
    //unsigned char *data;
    //int pcmreturn, l1, l2;
    //short s1, s2;
    //int frames;

    //data = (unsigned char *)malloc(periodsize);
    //frames = periodsize >> 2;
    
    //for(l1 = 0; l1 < 100; l1++) {
      ////for(l2 = 0; l2 < num_frames; l2++) {
      //for(l2 = 0; l2 < frames; l2++) {
        //s1 = (l2 % 128) * 100 - 5000;  
        //s2 = (l2 % 256) * 100 - 5000;  
        //data[4*l2] = (unsigned char)s1;
        //data[4*l2+1] = s1 >> 8;
        //data[4*l2+2] = (unsigned char)s2;
        //data[4*l2+3] = s2 >> 8;
      //}
      //while ((pcmreturn = snd_pcm_writei(pcm_handle, data, frames)) < 0) {
        //snd_pcm_prepare(pcm_handle);
        //fprintf(stderr, "<<<<<<<<<<<<<<< Buffer Underrun >>>>>>>>>>>>>>>\n");
      //}
    //}
    
    //sleep(1);
    
    //double frequency = 440.0, amplitude = 0.5;
    
    //for(l1 = 0; l1 < 100; l1++) {
      //for(l2 = 0; l2 < frames; l2++) {
        //s1 = random() - (RAND_MAX / 2); // white (?) noise
        //s2 = floor(amplitude*pow(2,depth)*sin(2*M_PI*(l1*frames+l2)/(rate/frequency))); // sine wave
        //data[4*l2] = (unsigned char)s1;
        //data[4*l2+1] = s1 >> 8;
        //data[4*l2+2] = (unsigned char)s2;
        //data[4*l2+3] = s2 >> 8;
      //}
      //while ((pcmreturn = snd_pcm_writei(pcm_handle, data, frames)) < 0) {
        //snd_pcm_prepare(pcm_handle);
        //fprintf(stderr, "<<<<<<<<<<<<<<< Buffer Underrun >>>>>>>>>>>>>>>\n");
      //}
    //}
    
    //sine_wave(pcm_handle, hwparams, 440.0, 0.5, 1.0);
    //usleep(500000);
    //for (int i=1; i<=5; i++)
    //    sine_wave(pcm_handle, hwparams, i*100.0, 0.5, 0.2);
    
    //sleep(1);
    //usleep(500000);
    float notes[] = {  16.35,   18.35,   20.60,   21.83,   24.50,   27.50,   30.87, 
                       32.70,   36.71,   41.20,   43.65,   49.00,   55.00,   61.74,
                       65.41,   73.42,   82.41,   87.31,   98.00,  110.00,  123.47,
                       130.81, 146.83,  164.81,  174.61,  196.00,  220.00,  246.94,
                      261.63,  293.66,  329.63,  349.23,  392.00,  440.00,  493.88,
                      523.25,  587.33,  659.25,  698.46,  783.99,  880.00,  987.77,
                     1046.50, 1174.66, 1318.51, 1396.91, 1567.98, 1760.00, 1975.53,
                     2093.00, 2349.32, 2637.02, 2793.83, 3135.96, 3520.00, 3951.07,
                     4186.01, 4698.63, 5274.04, 5587.65, 6271.93, 7040.00, 7902.13};
    // http://www.phy.mtu.edu/~suits/notefreqs.html
    
    /*
    char labeltext[10];
    for (int i=0; i<63; i++)
    {
        //printf("Sine: %.2f Hz\n", notes[i]);
        sprintf(labeltext, "%.2f Hz",notes[i]);
        gtk_label_set_text((GtkLabel *)pdata, labeltext);
        //gdk_window_invalidate_rect(window, NULL, TRUE);
        //gtk_widget_queue_draw_area(pdata,0,0,20,20);
        //gdk_window_process_updates(window, TRUE);
        gdk_window_process_all_updates();
        //sine_wave(pcm_handle, hwparams, notes[i], 0.5, 0.2);
        sine_wave(pcm_handle, hwparams, notes[i], 0.5, 0.1);
        //usleep(500000);
    }
    */
    //sine_wave(pcm_handle, hwparams, 500, 0.5, 0.5);
    //sine_wave(pcm_handle, 500, 0.5, 0.5);
    
    enum available_waveforms left_waveform = signal_params->left_waveform;
    float left_amplitude = signal_params->left_amplitude;
    float left_frequency = signal_params->left_frequency;
    float left_phase = signal_params->left_phase;
    float left_duty = signal_params->left_duty;
    enum available_waveforms right_waveform = signal_params->right_waveform;
    float right_amplitude = signal_params->right_amplitude;
    float right_frequency = signal_params->right_frequency;
    float right_phase = signal_params->right_phase;
    float right_duty = signal_params->right_duty;
    
    unsigned char *data;
    int pcmreturn, l1, l2;
    short s1, s2;
    int frames;
    unsigned int depth = 16;
    unsigned int rate;
    //double amplitude;
    if (left_amplitude > 1.0) left_amplitude = 1.0;
    else if (left_amplitude < -1.0) left_amplitude = -1.0;
    if (right_amplitude > 1.0) right_amplitude = 1.0;
    else if (right_amplitude < -1.0) right_amplitude = -1.0;
    
    
    
    snd_pcm_hw_params_t *hwparams;
    /* Allocate the snd_pcm_hw_params_t structure on the stack. */
    snd_pcm_hw_params_alloca(&hwparams);
    if (snd_pcm_hw_params_current(pcm_handle, hwparams) < 0)
    {
        g_message("Error retrieving current HW params.");
    }
    
    
    if (snd_pcm_hw_params_get_rate(hwparams, &rate, NULL) <0)
    {
        g_error("Error retrieving current rate.");
    }
    else g_message("Using rate: %d",rate);
    
    //depth = snd_pcm_hw_params_get_sbits(hwparams); // incorrect!!!
    
   
    
    snd_pcm_uframes_t periodsize;
    if (snd_pcm_hw_params_get_period_size(hwparams, &periodsize, NULL)<0)
    {
        g_message("Error retrieving period size.");
    }
    
    data = (unsigned char *)malloc(periodsize);
    //frames = periodsize >> 2;
    frames = periodsize / 4; // Same as ( periodsize >> 2)
    //double frequency = 440.0, amplitude = 0.5;
    
    /*
    for(l1 = 0; l1 < (length*rate/frames); l1++) {
      for(l2 = 0; l2 < frames; l2++) {
        s1 = floor(amplitude*pow(2,depth)*sin(2*M_PI*(l1*frames+l2)/(rate/frequency))); // sine wave
        s2 = floor(amplitude*pow(2,depth)*sin(2*M_PI*(l1*frames+l2)/(rate/frequency))); // sine wave
        data[4*l2] = (unsigned char)s1;
        data[4*l2+1] = s1 >> 8;
        data[4*l2+2] = (unsigned char)s2;
        data[4*l2+3] = s2 >> 8;
      }
      while ((pcmreturn = snd_pcm_writei(pcm_handle, data, frames)) < 0) {
        snd_pcm_prepare(pcm_handle);
        fprintf(stderr, "<<<<<<<<<<<<<<< Buffer Underrun >>>>>>>>>>>>>>>\n");
      }
    }
    */
    
    //amplitude = 0.3;
    float t = 0; // stops working when the increment becomes less that 10e5 of the value
    float left_time = 0, right_time = 0;
    //double t = 0; // must work for at least a day
    
    //float frequency = 440.0; // if the frequency is passed as an argument
    float time_step = 1.0/rate;
    int max_value = pow(2,depth-1); // 2^(bit depth - 1bit for sign) -- in case of signed signal
    //while (t < length)
    while (TRUE)
    {
        
        // Change the sound parameters on the fly
        left_waveform = signal_params->left_waveform;
        left_amplitude = signal_params->left_amplitude;
        left_frequency = signal_params->left_frequency;
        left_phase = signal_params->left_phase;
        left_duty = signal_params->left_duty;
        right_waveform = signal_params->right_waveform;
        right_amplitude = signal_params->right_amplitude;
        right_frequency = signal_params->right_frequency;
        right_phase = signal_params->right_phase;
        right_duty = signal_params->right_duty;
        
        
        
        for(l2 = 0; l2 < frames; l2++) 
        {
            t += time_step;
            //if (t >= 1.0/frequency) t = 0; // resetting time each period
            
            left_time += time_step;
            right_time += time_step;
            
            // resetting time after time_step becomes less than 1e-5 of t
            // at the first suitable end of period
            //if ((t >= 1e5*time_step) && \
            //   ((t - floor(t*frequency)/frequency) < time_step)) t = 0;
            if ((left_time >= 1e5*time_step) && \
               ((fmod(left_time,1/left_frequency)) < time_step)) left_time = 0;
            if ((right_time >= 1e5*time_step) && \
               ((fmod(right_time,1/right_frequency)) < time_step)) right_time = 0;
            
            
            
            
            if (left_waveform == SINE)           s1 = floor(left_amplitude*max_value*sine_wave(left_time, left_frequency, 0)); // sine wave
            else if (left_waveform == SAWTOOTH)  s1 = floor(left_amplitude*max_value*sawtooth_wave(left_time, left_frequency, 0)); // sawtooth wave
            else if (left_waveform == RAMP)      s1 = floor(left_amplitude*max_value*ramp_wave(left_time, left_frequency, 0)); // ramp wave
            else if (left_waveform == TRIANGLE)  s1 = floor(left_amplitude*max_value*triangle_wave(left_time, left_frequency, 0)); // triangle wave
            else if (left_waveform == SQUARE)    s1 = floor(left_amplitude*max_value*square_wave(left_time, left_frequency, 0)); // square wave
            else if (left_waveform == PULSE)     s1 = floor(left_amplitude*max_value*pulse_wave(left_time, left_frequency, 0, left_duty)); // pulse wave 
            else s1 = 0; // constant zero
            if (right_waveform == SINE)           s2 = floor(right_amplitude*max_value*sine_wave(right_time, right_frequency, 0)); // sine wave
            else if (right_waveform == SAWTOOTH)  s2 = floor(right_amplitude*max_value*sawtooth_wave(right_time, right_frequency, 0)); // sawtooth wave
            else if (right_waveform == RAMP)      s2 = floor(right_amplitude*max_value*ramp_wave(right_time, right_frequency, 0)); // ramp wave
            else if (right_waveform == TRIANGLE)  s2 = floor(right_amplitude*max_value*triangle_wave(right_time, right_frequency, 0)); // triangle wave
            else if (right_waveform == SQUARE)    s2 = floor(right_amplitude*max_value*square_wave(right_time, right_frequency, 0)); // square wave
            else if (right_waveform == PULSE)     s2 = floor(right_amplitude*max_value*pulse_wave(right_time, right_frequency, 0, right_duty)); // pulse wave 
            else s2 = 0; // constant zero
            //s2 = floor(amplitude*max_value*sine_wave(t, frequency, 0.0)); // sine wave TODO: independant signals for two channels
            data[4*l2] = (unsigned char)s1;
            data[4*l2+1] = s1 >> 8;
            data[4*l2+2] = (unsigned char)s2;
            data[4*l2+3] = s2 >> 8;
        }
        
        while ((pcmreturn = snd_pcm_writei(pcm_handle, data, frames)) < 0) 
        {
            snd_pcm_prepare(pcm_handle);
            fprintf(stderr, "<<<<<<<<<<<<<<< Buffer Underrun >>>>>>>>>>>>>>>\n");
        }
        
        if (pcmreturn < frames) g_message("%d of %d frames written.",pcmreturn,frames);
        //g_message("t = %f",t);
        //printf("t = %f\n",t);
        if (*signal_params->stop_sound) return;
    }
    
    g_message("---===---");    
    
    return;
}   

void alsa_close(snd_pcm_t *pcm_handle)
{
    g_message("alsa_close called");
    /* Stop PCM device and drop pending frames */
    snd_pcm_drop(pcm_handle);

    /* Stop PCM device after pending frames have been played */ 
    //snd_pcm_drain(pcm_handle);
    
    snd_pcm_close (pcm_handle);
    //gtk_button_set_label((GtkButton *)widget,"Play sounds");
    //exit (0);
    return;
}
 
 


static void
print_hello (GtkWidget *widget,
             gpointer   data)
{
  g_print ("Hello World\n");
}


void button_toggle(GtkWidget *widget, gpointer data)
{
    //GtkButton *Button = GTK_BUTTON(data);
    GtkWidget *Button = GTK_WIDGET(data);
    if (gtk_widget_get_sensitive(Button))
        gtk_widget_set_sensitive(Button, FALSE);
    else
        gtk_widget_set_sensitive(Button, TRUE);
    return;
}


static gpointer
thread_func( gpointer data )
{
    g_message("thread_func called");
    signal_params_t *signal_params;
    signal_params = (signal_params_t *) data;
    
    //fprintf(stderr, "New thread: stop_sound_global pointer: %lu\n",(unsigned long int) &stop_sound_global);
    
    //stop_sound = FALSE;
    *signal_params->stop_sound = FALSE;
    snd_pcm_t * pcm_handle = alsa_setup();
    
    g_message("WAVE: %d, FREQ: %f, ",signal_params->left_waveform,signal_params->left_frequency);
    
    //make_sound(pcm_handle, 
                //signal_params->left_waveform, 
                //signal_params->left_amplitude, 
                //signal_params->left_frequency, 
                //signal_params->left_phase, 
                //signal_params->left_duty,
                //signal_params->right_waveform, 
                //signal_params->right_amplitude, 
                //signal_params->right_frequency, 
                //signal_params->right_phase, 
                //signal_params->right_duty);
    
    make_sound(pcm_handle, signal_params);
    
    /*
    //while( TRUE )
    for (int i=0; i<10; i++)
    {
        make_sound(pcm_handle);
        //sleep( 1 );
        
        //button_toggle(NULL, data);
        //gdk_threads_enter();
        //gtk_button_set_label( GTK_BUTTON( data ), "Thread" );
        //gdk_threads_leave();
    }
    */
    
    alsa_close(pcm_handle);
    return( NULL );
}

void start_working_thread(GtkWidget *widget, gpointer signal_params)
{
    gtk_widget_set_sensitive(widget, FALSE);
    /* Create new thread */
    //printf("Creating new thread...");
    g_message("Creating new thread...");

    g_thread_new("Working Thread", thread_func, signal_params);

    return;
}

void stop_working_thread(GtkWidget *widget, gpointer data)
{
    /* Stop sound thread */
    g_message("Stopping audio thread...");
    stop_sound_global = TRUE;
    
    
    GtkWidget *startButton = GTK_WIDGET(data);
    gtk_widget_set_sensitive(startButton, TRUE);

    return;
}

void waveform_switch(GtkToggleButton *radiobutton, gpointer data)
{
    g_debug("waveform_switch called");
    if (!gtk_toggle_button_get_active(radiobutton)) return;
    
    //struct signal_params_t *signal_params = (struct signal_params_t *) data;
    signal_params_t *signal_params = (signal_params_t *) data;
    //struct signal_params_t *signal_params;
    //signal_params = data;
    
    //g_message("%d",signal_params->waveform);
    //g_message("%f",signal_params->frequency);
    //g_message("%f",signal_params->duty);
    //g_message("%d",((struct signal_params_t *)data)->waveform);
    //return;
    
    const gchar * sender_name = gtk_widget_get_name (GTK_WIDGET(radiobutton));
    
    if      (strcmp(sender_name,"left_Sine") == 0)      signal_params->left_waveform = SINE;
    else if (strcmp(sender_name,"left_Square") == 0)    signal_params->left_waveform = SQUARE;
    else if (strcmp(sender_name,"left_Sawtooth") == 0)  signal_params->left_waveform = SAWTOOTH;
    else if (strcmp(sender_name,"left_Ramp") == 0)      signal_params->left_waveform = RAMP;
    else if (strcmp(sender_name,"left_Triangle") == 0)  signal_params->left_waveform = TRIANGLE;
    else if (strcmp(sender_name,"left_Pulse") == 0)     signal_params->left_waveform = PULSE;
    else if (strcmp(sender_name,"right_Sine") == 0)     signal_params->right_waveform = SINE;
    else if (strcmp(sender_name,"right_Square") == 0)   signal_params->right_waveform = SQUARE;
    else if (strcmp(sender_name,"right_Sawtooth") == 0) signal_params->right_waveform = SAWTOOTH;
    else if (strcmp(sender_name,"right_Ramp") == 0)     signal_params->right_waveform = RAMP;
    else if (strcmp(sender_name,"right_Triangle") == 0) signal_params->right_waveform = TRIANGLE;
    else if (strcmp(sender_name,"right_Pulse") == 0)    signal_params->right_waveform = PULSE;
    else g_message("Unknown waveform");
    
    g_message("New waveform: %s",sender_name);
    
    //struct signal_params_t *signal_params = malloc(sizeof(struct signal_params_t));
    //const gchar *label = gtk_button_get_label(GTK_BUTTON(radiobutton));
    //g_message("%s",label);
    //if      (strcmp(label,"Sine") == 0)     signal_params->waveform = SINE;
    //else if (strcmp(label,"Square") == 0)   signal_params->waveform = SQUARE;
    //else if (strcmp(label,"Sawtooth") == 0) signal_params->waveform = SAWTOOTH;
    //else if (strcmp(label,"Ramp") == 0)     signal_params->waveform = RAMP;
    //else if (strcmp(label,"Triangle") == 0) signal_params->waveform = TRIANGLE;
    //else if (strcmp(label,"Pulse") == 0)    signal_params->waveform = PULSE;
    //else g_message("???");
    //g_message("New waveform: %d",signal_params->waveform);
    return;
}

void frequency_change(GtkWidget *widget, gpointer data)
{
    //gtk_widget_set_sensitive(widget, FALSE);
    /* Create new thread */
    //printf("Creating new thread...");
   // g_message("Creating new thread...");

    signal_params_t *signal_params;
    signal_params = (signal_params_t *) data;

    const gchar * sender_name = gtk_widget_get_name (GTK_WIDGET(widget));
    if (strcmp(sender_name,"left_frequency") == 0) 
        signal_params->left_frequency = gtk_spin_button_get_value(GTK_SPIN_BUTTON(widget));
    else if (strcmp(sender_name,"right_frequency") == 0)
        signal_params->right_frequency = gtk_spin_button_get_value(GTK_SPIN_BUTTON(widget));
    
    return;
}


void amplitude_change(GtkWidget *widget, gpointer data)
{
    signal_params_t *signal_params;
    signal_params = (signal_params_t *) data;
    
    const gchar * sender_name = gtk_widget_get_name (GTK_WIDGET(widget));
    if (strcmp(sender_name,"left_amplitude") == 0) 
        signal_params->left_amplitude = gtk_spin_button_get_value(GTK_SPIN_BUTTON(widget));
    else if (strcmp(sender_name,"right_amplitude") == 0)
        signal_params->right_amplitude = gtk_spin_button_get_value(GTK_SPIN_BUTTON(widget));
        
    //signal_params->amplitude = gtk_spin_button_get_value(GTK_SPIN_BUTTON(widget));
    return;
}

void duty_change(GtkWidget *widget, gpointer data)
{
    const gchar * sender_name = gtk_widget_get_name (GTK_WIDGET(widget));
    if (strcmp(sender_name,"left_duty") == 0) 
        ((signal_params_t *) data)->left_duty = 0.001 * gtk_spin_button_get_value(GTK_SPIN_BUTTON(widget));
    else if (strcmp(sender_name,"right_duty") == 0)
        ((signal_params_t *) data)->right_duty = 0.001 * gtk_spin_button_get_value(GTK_SPIN_BUTTON(widget));
    //((signal_params_t *) data)->duty = 0.001 * gtk_spin_button_get_value(GTK_SPIN_BUTTON(widget));
    return;
}

static void
activate (GtkApplication *app,
          gpointer        user_data)
{
    GtkWidget *window;
    GtkWidget *start_button, *stop_button;
    GtkWidget *button_box, *stack_box, *left_stack_box, *right_stack_box, *leftFrame, *rightFrame;
    GtkWidget *left_frequencyBox, *left_amplitudeBox, *left_dutyBox, *right_frequencyBox, *right_amplitudeBox, *right_dutyBox;
    GThread   *workingThread;
    //GtkRadioButton *sineButton, *squareButton, *sawtoothButton, *rampButton, *triangleButton, *pulseButton;
    GtkWidget *left_sineButton, *left_squareButton, *left_sawtoothButton;
    GtkWidget *left_rampButton, *left_triangleButton, *left_pulseButton;
    GtkWidget *right_sineButton, *right_squareButton, *right_sawtoothButton;
    GtkWidget *right_rampButton, *right_triangleButton, *right_pulseButton;
    //struct signal_params_t signal_params;
    //signal_params_t signal_params;
    //signal_params.waveform = SINE;
    //signal_params.frequency = 440.0;
    //signal_params.duty = 0.010;
    //fprintf(stderr,"waveform = %d\n",signal_params.waveform);
    signal_params_t *signal_params;
    signal_params = malloc(sizeof(signal_params_t));
    //fprintf(stderr, "stop_sound pointer: %lu\n",(unsigned long int)&stop_sound_global);
    
    // pass a pointer to the global stop variable, 
    // since the loacl one will be thread-local
    signal_params->stop_sound = &stop_sound_global; 
    signal_params->left_waveform = SINE;
    signal_params->left_amplitude = 0.9;
    signal_params->left_frequency = 1000.0;
    signal_params->left_phase = 0.0;
    signal_params->left_duty = 0.0001;
    signal_params->right_waveform = SINE;
    signal_params->right_amplitude = 0.9;
    signal_params->right_frequency = 1000.0;
    signal_params->right_phase = 0.0;
    signal_params->right_duty = 0.0001;
    //fprintf(stderr,"waveform = %d\n",signal_params->waveform);

    
    window = gtk_application_window_new (app);
    gtk_window_set_title (GTK_WINDOW (window), "GtkSignalGenerator v.0.1");
    gtk_window_set_default_size (GTK_WINDOW (window), 200, 200);

    //stack_box = gtk_layout_new (NULL, NULL);
    stack_box = gtk_grid_new();
    gtk_container_add (GTK_CONTAINER (window), stack_box);

    //button_box = gtk_button_box_new (GTK_ORIENTATION_HORIZONTAL);
    //button_box = gtk_button_box_new (GTK_ORIENTATION_VERTICAL);
    //gtk_container_add (GTK_CONTAINER (window), button_box);
    //gtk_container_add (GTK_CONTAINER (stack_box), button_box);



    //button = gtk_button_new_with_label ("Hello World");
    start_button = gtk_button_new_with_label ("Start");
    //g_object_set_property(G_OBJECT(start_button),"always-show-image", TRUE);
    stop_button = gtk_button_new_with_label ("Stop");

    //g_signal_connect (button, "clicked", G_CALLBACK (print_hello), NULL);
    //g_signal_connect (button, "clicked", G_CALLBACK (alsa_setup), NULL);
    //GtkWidget pointers[] = {window,textlabel};
    //g_signal_connect(start_button, "clicked", G_CALLBACK (alsa_setup), textlabel);
    //g_signal_connect(start_button, "clicked", G_CALLBACK (button_toggle), stop_button);
    g_signal_connect(start_button, "clicked", G_CALLBACK (start_working_thread), signal_params);
    
    //g_signal_connect_swapped (button, "clicked", G_CALLBACK (gtk_widget_destroy), window);
    //g_signal_connect_swapped (stop_button, "clicked", G_CALLBACK (gtk_widget_destroy), window);
    g_signal_connect(stop_button, "clicked", G_CALLBACK (stop_working_thread), start_button);
    //gtk_container_add (GTK_CONTAINER (button_box), start_button);
    //gtk_container_add (GTK_CONTAINER (button_box), stop_button);
    gtk_grid_attach(GTK_GRID(stack_box), start_button, 0, 0, 2, 1);
    gtk_grid_attach(GTK_GRID(stack_box), stop_button, 2, 0, 2, 1);
    //gtk_container_add (GTK_CONTAINER (stack_box), textlabel);



    // ****************
    // * LEFT CHANNEL *
    // ****************
    
    leftFrame = gtk_frame_new(" Left channel ");
    left_stack_box = gtk_grid_new();
    gtk_container_add (GTK_CONTAINER (leftFrame), left_stack_box);
    
    
    GtkWidget *textlabel = gtk_label_new("Frequency, Hz:");
    gtk_grid_attach(GTK_GRID(left_stack_box), textlabel, 0, 0, 1, 1);
    //frequencyBox = gtk_spin_button_new(NULL, 0.1, 4);
    left_frequencyBox = gtk_spin_button_new_with_range(10.0, 20000.0,10.0); // 10Hz to 20kHz with 10Hz step
    gtk_spin_button_set_value(GTK_SPIN_BUTTON(left_frequencyBox), signal_params->left_frequency);
    gtk_grid_attach(GTK_GRID(left_stack_box), left_frequencyBox, 1, 0, 1, 1);
    gtk_widget_set_name (GTK_WIDGET(left_frequencyBox), "left_frequency");
    g_signal_connect(left_frequencyBox, "value-changed", G_CALLBACK (frequency_change), signal_params);

    gtk_grid_attach(GTK_GRID(left_stack_box), gtk_label_new("Amplitude:"), 0, 1, 1, 1);
    left_amplitudeBox = gtk_spin_button_new_with_range(-1.0, 1.0,0.1);
    gtk_widget_set_name (GTK_WIDGET(left_amplitudeBox), "left_amplitude");
    gtk_spin_button_set_value(GTK_SPIN_BUTTON(left_amplitudeBox), signal_params->left_amplitude);
    gtk_grid_attach(GTK_GRID(left_stack_box), left_amplitudeBox, 1, 1, 1, 1);
    g_signal_connect(left_amplitudeBox, "value-changed", G_CALLBACK (amplitude_change), signal_params);

    gtk_grid_attach(GTK_GRID(left_stack_box), gtk_label_new("Duty cycle, ms:"), 0, 2, 1, 1);
    left_dutyBox = gtk_spin_button_new_with_range(0.05, 50.0,0.01);
    gtk_widget_set_name (GTK_WIDGET(left_dutyBox), "left_duty");
    gtk_spin_button_set_value(GTK_SPIN_BUTTON(left_dutyBox), 1000*signal_params->left_duty);
    gtk_grid_attach(GTK_GRID(left_stack_box), left_dutyBox, 1, 2, 1, 1);
    g_signal_connect(left_dutyBox, "value-changed", G_CALLBACK (duty_change), signal_params);  
    
    GtkWidget *left_box = gtk_box_new (GTK_ORIENTATION_VERTICAL, 2);
    gtk_box_set_homogeneous (GTK_BOX (left_box), TRUE);

    //GSList *waveformGroup = NULL;
    //GtkWidget *item;
    
    //left_sineButton = gtk_radio_button_new_with_label(waveformGroup, "Sine");
    left_sineButton = gtk_radio_button_new_with_label(NULL, "Sine");
    gtk_widget_set_name (GTK_WIDGET(left_sineButton), "left_Sine");
    g_signal_connect(left_sineButton, "toggled", G_CALLBACK (waveform_switch), signal_params);
    //waveformGroup = gtk_radio_button_get_group (GTK_RADIO_BUTTON (item));
    gtk_box_pack_start (GTK_BOX (left_box), left_sineButton, TRUE, TRUE, 2);
    
    left_squareButton = gtk_radio_button_new_with_label_from_widget(GTK_RADIO_BUTTON(left_sineButton), "Square");
    gtk_widget_set_name (GTK_WIDGET(left_squareButton), "left_Square");
    g_signal_connect(left_squareButton, "toggled", G_CALLBACK (waveform_switch), signal_params);
    gtk_box_pack_start (GTK_BOX (left_box), left_squareButton, TRUE, TRUE, 2);
    
    left_sawtoothButton = gtk_radio_button_new_with_label_from_widget(GTK_RADIO_BUTTON(left_sineButton), "Sawtooth");
    gtk_widget_set_name (GTK_WIDGET(left_sawtoothButton), "left_Sawtooth");
    g_signal_connect(left_sawtoothButton, "toggled", G_CALLBACK (waveform_switch), signal_params);
    gtk_box_pack_start (GTK_BOX (left_box), left_sawtoothButton, TRUE, TRUE, 2);
    
    left_rampButton = gtk_radio_button_new_with_label_from_widget(GTK_RADIO_BUTTON(left_sineButton), "Ramp");
    gtk_widget_set_name (GTK_WIDGET(left_rampButton), "left_Ramp");
    g_signal_connect(left_rampButton, "toggled", G_CALLBACK (waveform_switch), signal_params);
    gtk_box_pack_start (GTK_BOX (left_box), left_rampButton, TRUE, TRUE, 2);
    
    left_triangleButton = gtk_radio_button_new_with_label_from_widget(GTK_RADIO_BUTTON(left_sineButton), "Triangle");
    gtk_widget_set_name (GTK_WIDGET(left_triangleButton), "left_Triangle");
    g_signal_connect(left_triangleButton, "toggled", G_CALLBACK (waveform_switch), signal_params);
    gtk_box_pack_start (GTK_BOX (left_box), left_triangleButton, TRUE, TRUE, 2);
    
    left_pulseButton = gtk_radio_button_new_with_label_from_widget(GTK_RADIO_BUTTON(left_sineButton), "Pulse");
    gtk_widget_set_name (GTK_WIDGET(left_pulseButton), "left_Pulse");
    g_signal_connect(left_pulseButton, "toggled", G_CALLBACK (waveform_switch), signal_params);
    gtk_box_pack_start (GTK_BOX (left_box), left_pulseButton, TRUE, TRUE, 2);
    
    //gtk_container_add (GTK_CONTAINER (window), box);
    // Pack them into a box, then show all the widgets
    
    //gtk_box_pack_start (GTK_BOX (box), radio2, TRUE, TRUE, 2);
    gtk_grid_attach(GTK_GRID(left_stack_box), left_box, 0, 4, 1, 2);
    
    gtk_grid_attach(GTK_GRID(stack_box), leftFrame, 0, 1, 2, 1);
    
    
    
    // *****************
    // * RIGHT CHANNEL *
    // *****************
    
    rightFrame = gtk_frame_new(" Right channel ");
    right_stack_box = gtk_grid_new();
    gtk_container_add (GTK_CONTAINER (rightFrame), right_stack_box);
    
    
    gtk_grid_attach(GTK_GRID(right_stack_box), gtk_label_new("Frequency, Hz:"), 0, 0, 1, 1);
    //frequencyBox = gtk_spin_button_new(NULL, 0.1, 4);
    right_frequencyBox = gtk_spin_button_new_with_range(10.0, 20000.0,10.0); // 10Hz to 20kHz with 10Hz step
    gtk_spin_button_set_value(GTK_SPIN_BUTTON(right_frequencyBox), signal_params->right_frequency);
    gtk_grid_attach(GTK_GRID(right_stack_box), right_frequencyBox, 1, 0, 1, 1);
    gtk_widget_set_name (GTK_WIDGET(right_frequencyBox), "right_frequency");
    g_signal_connect(right_frequencyBox, "value-changed", G_CALLBACK (frequency_change), signal_params);

    gtk_grid_attach(GTK_GRID(right_stack_box), gtk_label_new("Amplitude:"), 0, 1, 1, 1);
    right_amplitudeBox = gtk_spin_button_new_with_range(-1.0, 1.0,0.1);
    gtk_widget_set_name (GTK_WIDGET(right_amplitudeBox), "right_amplitude");
    gtk_spin_button_set_value(GTK_SPIN_BUTTON(right_amplitudeBox), signal_params->right_amplitude);
    gtk_grid_attach(GTK_GRID(right_stack_box), right_amplitudeBox, 1, 1, 1, 1);
    g_signal_connect(right_amplitudeBox, "value-changed", G_CALLBACK (amplitude_change), signal_params);

    gtk_grid_attach(GTK_GRID(right_stack_box), gtk_label_new("Duty cycle, ms:"), 0, 2, 1, 1);
    right_dutyBox = gtk_spin_button_new_with_range(0.05, 50.0,0.01);
    gtk_widget_set_name (GTK_WIDGET(right_dutyBox), "right_duty");
    gtk_spin_button_set_value(GTK_SPIN_BUTTON(right_dutyBox), 1000*signal_params->right_duty);
    gtk_grid_attach(GTK_GRID(right_stack_box), right_dutyBox, 1, 2, 1, 1);
    g_signal_connect(right_dutyBox, "value-changed", G_CALLBACK (duty_change), signal_params);  
    
    GtkWidget *right_box = gtk_box_new (GTK_ORIENTATION_VERTICAL, 2);
    gtk_box_set_homogeneous (GTK_BOX (right_box), TRUE);

    //right_sineButton = gtk_radio_button_new_with_label(waveformGroup, "Sine");
    right_sineButton = gtk_radio_button_new_with_label(NULL, "Sine");
    gtk_widget_set_name (GTK_WIDGET(right_sineButton), "right_Sine");
    g_signal_connect(right_sineButton, "toggled", G_CALLBACK (waveform_switch), signal_params);
    //waveformGroup = gtk_radio_button_get_group (GTK_RADIO_BUTTON (item));
    gtk_box_pack_start (GTK_BOX (right_box), right_sineButton, TRUE, TRUE, 2);
    
    right_squareButton = gtk_radio_button_new_with_label_from_widget(GTK_RADIO_BUTTON(right_sineButton), "Square");
    gtk_widget_set_name (GTK_WIDGET(right_squareButton), "right_Square");
    g_signal_connect(right_squareButton, "toggled", G_CALLBACK (waveform_switch), signal_params);
    gtk_box_pack_start (GTK_BOX (right_box), right_squareButton, TRUE, TRUE, 2);
    
    right_sawtoothButton = gtk_radio_button_new_with_label_from_widget(GTK_RADIO_BUTTON(right_sineButton), "Sawtooth");
    gtk_widget_set_name (GTK_WIDGET(right_sawtoothButton), "right_Sawtooth");
    g_signal_connect(right_sawtoothButton, "toggled", G_CALLBACK (waveform_switch), signal_params);
    gtk_box_pack_start (GTK_BOX (right_box), right_sawtoothButton, TRUE, TRUE, 2);
    
    right_rampButton = gtk_radio_button_new_with_label_from_widget(GTK_RADIO_BUTTON(right_sineButton), "Ramp");
    gtk_widget_set_name (GTK_WIDGET(right_rampButton), "right_Ramp");
    g_signal_connect(right_rampButton, "toggled", G_CALLBACK (waveform_switch), signal_params);
    gtk_box_pack_start (GTK_BOX (right_box), right_rampButton, TRUE, TRUE, 2);
    
    right_triangleButton = gtk_radio_button_new_with_label_from_widget(GTK_RADIO_BUTTON(right_sineButton), "Triangle");
    gtk_widget_set_name (GTK_WIDGET(right_triangleButton), "right_Triangle");
    g_signal_connect(right_triangleButton, "toggled", G_CALLBACK (waveform_switch), signal_params);
    gtk_box_pack_start (GTK_BOX (right_box), right_triangleButton, TRUE, TRUE, 2);
    
    right_pulseButton = gtk_radio_button_new_with_label_from_widget(GTK_RADIO_BUTTON(right_sineButton), "Pulse");
    gtk_widget_set_name (GTK_WIDGET(right_pulseButton), "right_Pulse");
    g_signal_connect(right_pulseButton, "toggled", G_CALLBACK (waveform_switch), signal_params);
    gtk_box_pack_start (GTK_BOX (right_box), right_pulseButton, TRUE, TRUE, 2);
    
    //gtk_container_add (GTK_CONTAINER (window), box);
    // Pack them into a box, then show all the widgets
    
    //gtk_box_pack_start (GTK_BOX (box), radio2, TRUE, TRUE, 2);
    gtk_grid_attach(GTK_GRID(right_stack_box), right_box, 0, 4, 1, 2);
    
    gtk_grid_attach(GTK_GRID(stack_box), rightFrame, 2, 1, 2, 1);
    
    //gtk_container_add (GTK_CONTAINER (stack_box), leftFrame);

    gtk_widget_show_all (window);
}

int
main (int    argc,
      char **argv)
{    
    //fprintf(stderr, "stop_sound_global pointer: %lu\n",(unsigned long int) &stop_sound_global);
    
    GtkApplication *app;
    int status;

    // G_APPLICATION_NON_UNIQUE is necessary in order to start several copies of
    // the program which DO NOT use the same memory for their variables.
    // Otherwise arbitrary "overlapping" conditions occur
    app = gtk_application_new ("wombat.GtkSignalGenerator", G_APPLICATION_NON_UNIQUE);
    g_signal_connect (app, "activate", G_CALLBACK (activate), NULL);
    status = g_application_run (G_APPLICATION (app), argc, argv);
    g_object_unref (app);

    return status;
}
 
 
 
 
 
 
 
 
 
